#!/usr/bin/python3.5
#-*- coding:utf-8 -*-
import os
from string import ascii_uppercase
from regle import Regle
from os import listdir
from os.path import isfile, join
from itertools import product

class Action():
    def __init__(self, nomRepertoire, regle):
        self.nomRepertoire = nomRepertoire
        self.regle = regle

    def get_nomRepertoire(self):
        return self.nomRepertoire

    def set_nomRepertoire(self, nomRepertoire):
        self.nomRepertoire = nomRepertoire

    def get_regle(self):
        return self.regle

    def set_regle(self, regle):
        self.regle = regle

    def simule(self):
        fileNames = [f for f in listdir(self.nomRepertoire) if isfile(join(self.nomRepertoire, f))]
        if self.regle.get_extension() != None:
            fileNamesTmp = []
            for fileName in fileNames:
                if os.path.splitext(fileName)[1] in self.regle.get_extension():
                    fileNamesTmp.append(fileName)
            fileNames = fileNamesTmp
        renamedFiles = []
        for fileName in fileNames:
            fileNameTmp = os.path.splitext(fileName)
            renamed = fileNameTmp[0]
            if self.regle.get_nomDuFichier():
                renamed = self.regle.get_nomDuFichier()
            if self.regle.get_prefixe():
                renamed = self.regle.get_prefixe() + renamed
            if self.regle.get_postfixe():
                renamed = renamed + self.regle.get_postfixe()
            renamedFiles.append(renamed + fileNameTmp[1])
        if self.regle.get_amorce().strip() == 'chiffre':
            renamedFilesTmp = []
            i=1
            if self.regle.get_apartirde():
                i = int(self.regle.get_apartirde())
            for renamedFile in renamedFiles:
                renamedFile = '{0:03}'.format(i) + renamedFile
                renamedFilesTmp.append(renamedFile)
                i+=1
            renamedFiles = renamedFilesTmp
        elif self.regle.get_amorce().strip() == 'lettre':
            i=0
            rep = int(len(renamedFiles) / 26) + 1
            if self.regle.get_apartirde():
                rep = len(self.regle.get_apartirde())+int(len(renamedFiles) / 26)
            isStarted = False
            for combo in product(ascii_uppercase, repeat=rep):
                if self.regle.get_apartirde():
                    if isStarted == False and self.regle.get_apartirde() == ''.join(combo):
                        renamedFiles[i] = ''.join(combo) + renamedFiles[i]
                        isStarted = True
                    elif isStarted:
                        renamedFiles[i] = ''.join(combo) + renamedFiles[i]
                    else:
                        continue
                    i+=1
                else:
                    renamedFiles[i] = ''.join(combo) + renamedFiles[i]
                    i+=1
                if i == len(renamedFiles):
                    break
        return {'fileNames' : fileNames, 'renamedFiles' : renamedFiles}
