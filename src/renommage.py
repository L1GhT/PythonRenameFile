#!/usr/bin/python3.5
#-*- coding:utf-8 -*-

from action import Action
import os

class Renommage(Action):
    def __init__(self, nomRepertoire, regle):
        super().__init__(nomRepertoire, regle)

    def renommer(self, simulList):
        for filename, renamedfilename in zip(simulList['fileNames'], simulList['renamedFiles']):
            os.rename(self.nomRepertoire + filename, self.nomRepertoire + renamedfilename)
