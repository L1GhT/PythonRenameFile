#!/usr/bin/python3.5
#-*- coding:utf-8 -*-
from regle import Regle
from listeregle import ListeRegle
from action import Action

def main():
    # Init first rule
    regle = Regle('lettre', 'AAAA', None, True, 'swag', None, None)
    # Test if the __str__ is working
    print(regle) # Yes it is :-)
    # Init the rule list
    listeregles = ListeRegle()
    # Adding a rule object to the listergle
    listeregles.set_regles([regle])
    # Trying to save the first rule object
    listeregles.sauvegarder()
    # Adding another config with 2 rules this time
    listeregles.set_regles([regle, Regle('testagain', 'coucou', 'bonjour', False, 'hey', 'brother', 'fock')])
    # Testing the save on multi lines
    listeregles.sauvegarder()
    # Get the str object value with 2 rules
    print(listeregles) # It's working again :D
    # Now trying to setup an action
    action = Action('/home/l1ght/test/', regle)
    # Now simulate it please !
    print(action.simule())


if __name__ == '__main__':
    main()
