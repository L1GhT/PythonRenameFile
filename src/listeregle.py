#!/usr/bin/python3.5
#-*- coding:utf-8 -*-

from regle import Regle

class ListeRegle():
    def __init__(self):
        self.regles = []

    def get_regles(self):
        return self.regles

    def set_regles(self, regles):
        self.regles = regles

    def sauvegarder(self):
        with open('../conf/rules.init', 'w') as initFile:
            for regle in self.regles:
                initFile.write("%s\n" % regle)
        return True

    def charger(self):
        reglesTmp = []
        with open('../conf/rules.init', 'r') as initFile:
            for line in initFile:
                regle = line[:-1].split(', ')
                if regle[3] == 'True':
                    regle[3] = True
                elif regle[3] == 'False':
                    regle[3] = False
                else:
                    continue
                reglesTmp.append(Regle(regle[0], regle[1], regle[2], regle[3], regle[4], regle[5], regle[6]))
        self.regles = reglesTmp
        return self.regles

    def __str__(self):
        return str(['('+str(x)+')' for x in self.regles])
