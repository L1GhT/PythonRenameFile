# Simple enough, just import everything from tkinter.
from tkinter import *
from tkinter import messagebox
from action import Action
from renommage import Renommage
from regle import Regle
import tkinter.ttk as ttk
import itertools
from listeregle import ListeRegle
# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):

        # parameters that you want to send through the Frame class.
        Frame.__init__(self, master)

        #reference to the master widget, which is the tk window
        self.master = master

        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget
        self.master.title("Rename files !")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Creer", command=self.save_rule)
        submenu = Menu(file)
        i=0
        listeregle = ListeRegle()
        for regle in listeregle.charger():
            submenu.add_command(variable=regle, label="Regle " + str(i))
            i+=1
        file.add_cascade(label="Regles", menu=submenu, underline=0)        #added "file" to our menu
        menu.add_cascade(label="Regles", menu=file)

        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="About", command=self.about)

        #added "file" to our menu
        menu.add_cascade(label="?", menu=edit)


        L = ['Aucun','Lettre','Chiffre']
        self.choice = L[0]
        self.vS = StringVar(master=self)
        self.vS.set(L[0])
        self.oS = OptionMenu(self,self.vS,*L, command=self.choice_box)
        self.oS.grid(row=33, column=0)

        self.bouton_renommer = Button(self, text="Renommer", command=self.rename)
        self.bouton_renommer.grid(row=40, column=4)

        self.v = IntVar()
        self.v.set(1)

        self.bouton_nomoriginal = Radiobutton(self, text="Nom original", variable=self.v, value=1)
        self.bouton_nomoriginal.grid(row=32, column=2)

        self.bouton_nomfichier = Radiobutton(self, text="Nom personnalisé", variable=self.v, value=2)
        self.bouton_nomfichier.grid(row=33, column=2)


        self.label_nomrepertoire = Label(self, text="Nom du répertoire", width=20, height=6)
        self.label_nomrepertoire.grid(row=5, column=1)

        self.label_renommerenlots = Label(self, text="Renommer en lots")
        self.label_renommerenlots.grid(row=4, column=2)

        self.label_amorce = Label(self, text="Amorce")
        self.label_amorce.grid(row=31, column=0)

        self.label_prefixe = Label(self, text="Préfixe")
        self.label_prefixe.grid(row=31, column=1)

        self.label_nomfichier = Label(self, text="Nom du fichier")
        self.label_nomfichier.grid(row=31, column=2)

        self.label_postfixe = Label(self, text="Postfixe", width=30)
        self.label_postfixe.grid(row=31, column=3)

        self.label_extension = Label(self, text="Extension Concernée")
        self.label_extension.grid(row=31, column=4)

        self.label_apartirde = Label(self, text="A partir de")
        self.label_apartirde.grid(row=50, column=0)

        self.entry_repertoire = Entry(self)
        self.entry_repertoire.grid(row=5, column=2)

        self.entry_prefixe = Entry(self)
        self.entry_prefixe.grid(row=32, column=1)

        self.entry_postfixe = Entry(self)
        self.entry_postfixe.grid(row=32, column=3)

        self.entry_apartirde = Entry(self)
        self.entry_apartirde.grid()

        self.entry_nomfichier = Entry(self)
        self.entry_nomfichier.grid(row=34, column=2)

        self.entry_extension = Entry(self)
        self.entry_extension.grid(row=32, column=4)

    def save_rule(self):
        regle = self.get_rule()
        listeRegle = ListeRegle()
        regles = listeRegle.charger()
        if regles == None:
            regles = []
        print(regle)
        regles.append(regle)
        listeRegle.sauvegarder()


    def client_exit(self):
        exit()

    def about(self):
        print('hi')

    def choice_box(self, choice):
        self.choice = choice

    def alert_window(self):
        messagebox.showerror("WARNING", "Element missing or invalid value")

    def new_window(self, files):
        self.newWindow = Toplevel(self.master)
        self.tree = ttk.Treeview(self.newWindow, columns=('zero', 'one'))
        self.tree.heading('zero', text='Nom du fichier')
        self.tree.heading('one', text='Nom du fichier renommer')
        self.tree.column('zero')
        self.tree.column('one')
        self.tree.grid(row=0, column=0, sticky='nsew')
        self.tree['show'] = 'headings'
        i=0
        for fileName, renamedFileName in zip(files['fileNames'],files['renamedFiles']):
            self.tree.insert('', '0', values=(fileName, renamedFileName))
            i+=1
        self.button_tree = Button(self.newWindow, text="Ok", command=self.do_rename)
        self.button_tree.grid(row=i+1, column=1)


    def get_rule(self):
        amorce = self.choice
        aPartirde = None
        if self.entry_apartirde.get().strip() != "" and amorce.strip().lower() == "lettre":
            aPartirde = self.entry_apartirde.get().upper()
        elif self.entry_apartirde.get().strip() != "":
            try:
                aPartirde = int(self.entry_apartirde.get())
            except ValueError:
                self.alert_window()


        prefix = None
        if self.entry_prefixe.get().strip() != "":
            prefix = self.entry_prefixe.get()

        postfixe = None
        if self.entry_postfixe.get().strip() != "":
            postfixe = self.entry_postfixe.get()

        nomFichier = False
        nomDuFichier = None
        if self.v.get() == 2 and self.entry_nomfichier.get().strip() != "":
            nomDuFichier = self.entry_nomfichier.get()
        elif self.v.get() == 2 and self.entry_nomfichier.get().strip() == "":
            self.alert_window()
        extension = None
        if self.entry_extension.get().strip() != "":
            extension = self.entry_extension.get().split(', ')

        return Regle(amorce.lower(), aPartirde, prefix, nomFichier, nomDuFichier, postfixe, extension)

    def rename(self):
        regle = self.get_rule()
        self.nomRepertoire = self.entry_repertoire.get()
        if self.entry_repertoire.get().strip() == "":
            self.alert_window()
        self.renommage = Renommage(self.nomRepertoire, regle)
        self.simuleResult = self.renommage.simule()
        self.new_window(self.simuleResult)


    def do_rename(self):
        self.renommage.renommer(self.simuleResult)
        self.newWindow.destroy()

# root window created. Here, that would be the only window, but
# you can later have windows within windows.

if __name__ == '__main__':
    root = Tk()
    root.geometry("1000x400")
    #creation of an instance
    app = Window(root)
    #mainloop
    root.mainloop()
