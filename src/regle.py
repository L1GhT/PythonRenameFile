#!/usr/bin/python3.5
#-*- coding:utf-8 -*-

class Regle():
    def __init__(self, amorce, apartirde, prefixe, nomFichier, nomDuFichier, postfixe, extension):
        self.amorce = amorce
        self.apartirde = apartirde
        self.prefixe = prefixe
        self.nomFichier = nomFichier
        self.nomDuFichier = nomDuFichier
        self.postfixe = postfixe
        self.extension = extension

    def get_amorce(self):
        return self.amorce

    def set_amort(self, amorce):
        self.amorce = amorce

    def get_apartirde(self):
        return self.apartirde

    def set_apartirde(self, apartirde):
        self.apartirde = apartirde

    def get_prefixe(self):
        return self.prefixe

    def set_prefixe(self, prefixe):
        self.prefixe = prefixe

    def get_nomFichier(self):
        return self.nomFichier

    def set_nomFichier(self, nomFichier):
        self.nomFichier = nomFichier

    def get_nomDuFichier(self):
        return self.nomDuFichier

    def set_nomDuFichier(self, nomDuFichier):
        self.nomDuFichier = nomDuFichier

    def get_postfixe(self):
        return self.postfixe

    def set_postfixe(self, postfixe):
        self.postfixe = postfixe

    def get_extension(self):
        return self.extension

    def set_extension(self, extension):
        self.extension = extension

    def __str__(self):
        return "%s, %s, %s, %s, %s, %s, %s" % (self.amorce, self.apartirde, self.prefixe, self.nomFichier, self.nomDuFichier, self.postfixe, self.extension)
